public class Balls {

	enum Color {
		green, red
	}

	public static void main(String[] param) {
		// for debugging
	}

	public static void reorder(Color[] balls) {
		// TODO!!! Your program here
		int i = 0;
	    
		for (Color ball : balls)
			if (ball.ordinal() > 0)
				i++;
		
		for (int j = 0; j < i; j++)
			balls[j] = Color.red;
		
		for (int j = i; j < balls.length; j++)
			balls[j] = Color.green;
	}
}
